# Notflix 2

Please check out [the original Notflix](https://github.com/Bugswriter/notflix)

[![asciicast](https://asciinema.org/a/496978.svg)](https://asciinema.org/a/496978)

## Examples:
> Replace `notflix2` with `cargo run --` if you don't have Notflix2 in your PATH
```sh
# Get the first search result of 'The Social Network' and enable verbose printing
notflix2 -v 'The Social Network' 0
```
```sh
# Interactively select the nth result of 'Squid Game'
notflix2 'Squid Game'
```
```sh
# Run Notflix2 in fully interactive mode
notflix2
```
