use scraper::{element_ref::Text, ElementRef};

pub trait GetText {
    fn get_all_text(&mut self) -> String;
    fn get_first_text(&mut self) -> Option<String>;
}

impl GetText for Text<'_> {
    fn get_all_text(&mut self) -> String {
        self.collect::<Vec<_>>().join("")
    }
    fn get_first_text(&mut self) -> Option<String> {
        self.next().map(|text| text.to_string())
    }
}

impl GetText for ElementRef<'_> {
    fn get_all_text(&mut self) -> String {
        self.text().get_all_text()
    }
    fn get_first_text(&mut self) -> Option<String> {
        self.text().get_first_text()
    }
}
