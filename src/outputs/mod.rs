use self::peerflix::open_peerflix;
use crate::{
    inputs::args,
    scrape::{result::SearchResult, selectors::Selectors, web_requests::get_magnet_link},
};
use std::process::exit;

pub mod peerflix;

pub async fn output_data(
    args: args::Args,
    result: &SearchResult,
    selectors: Selectors,
) -> Result<(), anyhow::Error> {
    if args.mode == args::Mode::Print1337xSearchData {
        println!("{}", serde_json::to_string(&result)?);
        exit(0)
    }
    if args.verbose {
        eprintln!("Getting magnet link...");
    }
    let magnet = get_magnet_link(&args, &result, &selectors).await?;
    match args.mode {
        args::Mode::PrintMagnetLink => {
            println!("{}", magnet);
            Ok(())
        }
        args::Mode::OpenPeerflix => open_peerflix(magnet),
        _ => unreachable!(),
    }
}
