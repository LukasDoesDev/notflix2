#[cfg(unix)]
pub fn open_peerflix(magnet: String) -> Result<(), anyhow::Error> {
    eprintln!("Open peerflix (Execvp)");
    let err = exec::Command::new("peerflix")
        .args(&["-l", "-k"])
        .arg(magnet)
        .exec();
    Err(anyhow::anyhow!("Failed to open peerflix: {}", err))
}

#[cfg(not(unix))]
pub fn open_peerflix(magnet: String) -> Result<(), anyhow::Error> {
    use anyhow::Context;
    use std::process::{Command, Stdio};

    eprintln!("Open peerflix (Not execvp)");
    let output = Command::new("peerflix")
        .args(["-l", "-k"])
        .arg(magnet)
        .stderr(Stdio::inherit())
        .stdout(Stdio::inherit())
        .stdin(Stdio::inherit())
        .output()
        .context("Failed to execute command")?;
    if !output.status.success() {
        return Err(anyhow::anyhow!(
            "Command failed with exit code {}",
            output.status.code().unwrap()
        ));
    }
    Ok(())
}
