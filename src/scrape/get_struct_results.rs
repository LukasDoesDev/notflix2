use super::{result::SearchResult, selectors::Selectors};
use crate::get_all_text::GetText;
use scraper::Html;

pub fn get_search_results_struct(resp: String, selectors: &Selectors) -> Vec<SearchResult> {
    let doc = Html::parse_document(&resp);
    // TODO: get the sizes too
    doc.select(&selectors.item)
        .flat_map(|a| {
            let (name, url) = {
                let name_el = a.select(&selectors.name).next()?;
                let name = name_el.text().get_all_text();
                let url = name_el.value().attr("href")?.to_string();
                (name, url)
            };
            let size = {
                let size_el = a.select(&selectors.size).next()?;
                let size = size_el.text().get_all_text();
                size
            };
            let uploader = {
                let uploader_el = a.select(&selectors.uploader).next()?;
                let uploader = uploader_el.text().get_all_text();
                uploader
            };
            Some(SearchResult {
                name,
                url,
                size,
                uploader,
            })
        })
        .collect::<Vec<_>>()
}
