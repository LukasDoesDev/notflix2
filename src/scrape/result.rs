use serde::Serialize;

#[derive(Debug, Serialize)]
pub struct SearchResult {
    pub name: String,
    pub url: String,
    pub size: String,
    pub uploader: String,
}
