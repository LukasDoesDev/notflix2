use crate::{inputs::args, inputs::get_name, Selectors};
use anyhow::Context;
use scraper::Html;

use super::result::SearchResult;

pub async fn get_search_results(args: &args::Args) -> Result<String, anyhow::Error> {
    let search_name = get_name(&args.name)
        .context("Failed to get name")
        .map_err(|e| {
            eprintln!(); // Display the error after the prompt, not next to it
            e
        })?;
    if search_name == "" {
        return Err(anyhow::anyhow!("No name provided"));
    }
    let search_query = search_name.replace(" ", "+");
    if args.verbose {
        eprintln!(
            r#"Searching for video "{}" with query "{}"..."#,
            search_name, search_query
        )
    }
    let resp = reqwest::get(format!("{}/search/{}/1/", args.website, search_query))
        .await?
        .text()
        .await?;
    Ok(resp)
}

pub async fn get_magnet_link(
    args: &args::Args,
    result: &SearchResult,
    selectors: &Selectors,
) -> anyhow::Result<String> {
    let resp = reqwest::get(format!("{}{}", args.website, result.url))
        .await?
        .text()
        .await?;
    let doc = Html::parse_document(&resp);
    Ok(doc
        .select(&selectors.magnet)
        .next()
        .ok_or_else(|| anyhow::anyhow!("Unable to get a magnet link button"))?
        .value()
        .attr("href")
        .ok_or_else(|| {
            anyhow::anyhow!("Unable to get the href property of the magnet link button")
        })?
        .to_string())
}
