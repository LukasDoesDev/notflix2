use scraper::Selector;

pub struct Selectors {
    pub magnet: Selector,
    pub item: Selector,
    pub name: Selector,
    pub size: Selector,
    pub uploader: Selector,
}
