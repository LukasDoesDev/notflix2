use clap::Parser;
use inputs::get_result_idx;
use outputs::output_data;
use scrape::{
    get_struct_results::get_search_results_struct, selectors::Selectors,
    web_requests::get_search_results,
};
use scraper::Selector;

pub mod get_all_text;
pub mod inputs;
pub mod outputs;
pub mod scrape;

#[tokio::main]
async fn main() -> anyhow::Result<()> {
    let args = inputs::args::Args::parse();

    let selectors = Selectors {
        magnet: Selector::parse(r#"a[href^="magnet:?xt="]"#).unwrap(),
        item: Selector::parse("table.table-list > tbody > tr").unwrap(),
        name: Selector::parse("td.coll-1.name > a:not(.icon)").unwrap(),
        size: Selector::parse("td.coll-4.size").unwrap(),
        uploader: Selector::parse("td.coll-5").unwrap(),
    };

    let resp = get_search_results(&args).await?;

    let results = get_search_results_struct(resp, &selectors);
    if results.len() == 0 {
        return Err(anyhow::anyhow!("No results found"));
    }
    if args.verbose {
        eprintln!("Got results: {:#?}", results)
    }
    let result_idx = get_result_idx(&results, args.result_idx)?;
    let result = results
        .get(result_idx)
        .ok_or_else(|| anyhow::anyhow!("Unmatched result index"))?
        .clone();
    if args.verbose {
        eprintln!("Using result: {:?}", result)
    }

    output_data(args, result, selectors).await
}
