use requestty::Question;

use crate::scrape::result::SearchResult;

pub mod args;

pub fn get_name(name: &Option<String>) -> anyhow::Result<String> {
    match name {
        Some(name) => Ok(name.clone()),
        None => {
            let name_question = Question::input("name")
                .message("What is the search term you want to use?")
                .build();
            match requestty::prompt_one(name_question) {
                Ok(name) => Ok(name
                    .as_string()
                    .ok_or_else(|| anyhow::anyhow!("Question was not of type Answer::String"))?
                    .to_string()),
                Err(err) => Err(anyhow::anyhow!("{}", err)),
            }
        }
    }
}

pub fn get_result_idx(
    results: &Vec<SearchResult>,
    result_idx: Option<usize>,
) -> anyhow::Result<usize> {
    match result_idx {
        Some(result_idx) => Ok(result_idx),
        None => {
            let name_question = Question::select("name")
                .message("What is the video's name?")
                .choices(results.iter().enumerate().map(|(index, result)| {
                    format!(
                        r#"{}: "{}" ({}) by {}"#,
                        index, result.name, result.size, result.uploader
                    )
                }))
                .build();
            match requestty::prompt_one(name_question) {
                Ok(name) => {
                    let num = name
                        .as_list_item()
                        .ok_or_else(|| {
                            anyhow::anyhow!("Question was not of type Answer::ListItem")
                        })?
                        .index;
                    Ok(num)
                }
                Err(err) => Err(anyhow::anyhow!("{}", err)),
            }
        }
    }
}
