use clap::{ArgEnum, Parser};

/// Simple program to greet a person
#[derive(Parser, Debug)]
#[clap(author, version, about, long_about = None)]
pub struct Args {
    /// The name of the video to search for
    pub name: Option<String>,

    /// A predetermined search result index
    pub result_idx: Option<usize>,

    /// 1337x-compatible website to search
    #[clap(short, long, default_value = "https://1337x.to")]
    pub website: String,

    /// Whether the program should print each step in the process
    #[clap(short, long)]
    pub verbose: bool,

    /// Whether the program should print the 1337x data in json format or just the magnet link
    #[clap(short, long, default_value = "open-peerflix", arg_enum)]
    pub mode: Mode,
}

#[derive(Copy, Clone, PartialEq, Eq, PartialOrd, Ord, ArgEnum, Debug)]
pub enum Mode {
    Print1337xSearchData,
    PrintMagnetLink,
    OpenPeerflix,
}
